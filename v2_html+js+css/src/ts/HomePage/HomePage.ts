namespace namespaceHomePage {
    import TextLabel = namespaceCoreModule.TextLabel;
    import Page = namespaceCoreModule.Page;
    import Input = namespaceCoreModule.Input;
    import BaseElement = namespaceCoreModule.BaseElement;

    export class HomePage extends Page {

        public static getJavascriptFiles(): string[] {
            const jsList: string[] = super.getJavascriptFiles();

            return jsList;
        }

        protected getStyleList(): string[] {
            return [
            "HomePage/HomePage.css",
            ];
        }

        constructor() {
            super();
            this.addClass("HomePage");
        }

        protected build() {
            const header = new TextLabel();
            header.setText("HomePage");
            header.addClass("HomePage_HomeTitle");
            this.appendChild(header);
        }
    }
}
