import LayoutModule = Layout.LayoutModule;

let layoutModule: LayoutModule;

/**
 * Called as soon as the window has loaded
 */
window.onload = () => {
    if (!layoutModule) {
        layoutModule = new LayoutModule();
    }
    layoutModule.instantiatePage(window.location.hash);
};

/**
 * If the hash changes, then navigate to that page
 */
window.onhashchange = () => {
    layoutModule.navigate(window.location.hash);
};

function changePage(newHash: string): void {
    if (window.location.hash.replace("#", "") !== newHash) {
        layoutModule.navigate(newHash);
    }
}
