namespace namespaceTopBar {
    import BaseElement = namespaceCoreModule.BaseElement;
    import TextLabel = namespaceCoreModule.TextLabel;
    import Page = namespaceCoreModule.Page;

    export class TopBarModule extends Page {

        public constructor() {
            super();
            this.addClass("TopBarModule");
        }

        protected getStyleList(): string[] {
            return [
                "CoreModule/CoreModule.css",
                "TopBarModule/TopBarModule.css",
            ];
        }

        protected build() {
            const headerLeft = new BaseElement();
            headerLeft.addClass("TopBarModule_HeaderLeft");
            const leftTitle = new TextLabel();
            leftTitle.setText("Title");
            const subTitle = new TextLabel();
            subTitle.setText("Sub Title");
            headerLeft.appendChild(leftTitle);
            headerLeft.appendChild(subTitle);
            this.appendChild(headerLeft);

            this.createNavLinks();
        }

        private createNavLinks() {
            const headerRight = new BaseElement();
            headerRight.addClass("TopBarModule_HeaderRight");
            const homeLink = new TextLabel();
            homeLink.addClass("TopBarModule_HeaderRight-navElement");
            homeLink.setText("Home");
            homeLink.addEventListener("click", () => { changePage("home"); });
            headerRight.appendChild(homeLink);

            const navElement2 = new TextLabel();
            navElement2.addClass("TopBarModule_HeaderRight-navElement");
            navElement2.setText("Element2");
            headerRight.appendChild(navElement2);
            const navElement3 = new TextLabel();
            navElement3.addClass("TopBarModule_HeaderRight-navElement");
            navElement3.setText("Element3");
            headerRight.appendChild(navElement3);
            this.appendChild(headerRight);
        }
    }
}
