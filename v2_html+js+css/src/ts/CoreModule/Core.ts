namespace ns_core {

    export function ChangePageHash(link: string) {
        document.location.hash = link;
    }

    export function LoadJson(pathName: string, callbackFunction: (param: any) => void) {
        const xmlRequest: XMLHttpRequest = new XMLHttpRequest();
        xmlRequest.overrideMimeType("application/json");
        xmlRequest.open("GET", pathName, true);
        xmlRequest.onreadystatechange = () => {
            if (xmlRequest.readyState === 4 && xmlRequest.status === 200) {
                callbackFunction(JSON.parse(xmlRequest.responseText));
            }
        };
        xmlRequest.send(null);
    }

    export function LoadJsonWithScope(pathName: string, scope: any, callbackFunction: any) {
        const xmlRequest: XMLHttpRequest = new XMLHttpRequest();
        xmlRequest.overrideMimeType("application/json");
        xmlRequest.open("GET", pathName, true);
        xmlRequest.onreadystatechange = () => {
            if (xmlRequest.readyState === 4 && xmlRequest.status === 200) {
                callbackFunction(scope, JSON.parse(xmlRequest.responseText ));
            }
        };
        xmlRequest.send(null);
    }
}
