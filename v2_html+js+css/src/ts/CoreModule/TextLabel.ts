namespace namespaceCoreModule {

    import BaseElement = namespaceCoreModule.BaseElement;

    export class TextLabel extends BaseElement {

        public setText(text: string) {

            this.element.innerText = text;

        }

    }

}
