namespace namespaceCoreModule {

    import BaseElement = namespaceCoreModule.BaseElement;

    export class Page extends BaseElement {

        public static getJavascriptFiles(): string[] {
            return [];
        }

        /**
         *
         */
        constructor() {
            super();
            this.installStyles();
            this.addClass("CoreModule_Page");
            this.build();
        }

        /**
         * Cleans up all the child elements for the module being called
         */
        public cleanUp() {

            // tslint:disable-next-line: prefer-for-of
            for (let index = 0; index < this.element.children.length; index++) {
                const child = this.element.children[index];
                this.element.removeChild(child);
            }

        }

        protected getStyleList(): string[] {
            return [];
        }

        protected build() {
            // Must Override
        }

        protected installStyles() {
            for (const style of this.getStyleList()) {
                //
                const styleLink = document.createElement("link");
                styleLink.rel = "stylesheet";
                styleLink.type = "text/css";
                styleLink.href = "src/css/" + style;

                document.head.appendChild(styleLink);
            }
        }
    }
}
