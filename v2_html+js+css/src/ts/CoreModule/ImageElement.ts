namespace namespaceCoreModule {

    import BaseElement = namespaceCoreModule.BaseElement;

    export class ImageElement extends BaseElement {

        protected element: HTMLImageElement;

        constructor() {
            super("img");
        }

        public source(text: string) {

            this.element.src = text;

        }

    }

}
