namespace namespaceCoreModule {

    import BaseElement = namespaceCoreModule.BaseElement;

    export class Input extends BaseElement {

        protected element: HTMLInputElement;

        constructor() {
            super("input");
        }

        public setPlaceholder(text: string) {

            this.element.placeholder = text;

        }

    }

}
