namespace namespaceCoreModule {

    export class BaseElement {

        protected element: HTMLElement;

        /**
         * Creates the internal element with an overridden type if necessary
         * @param type type of item to make, defaults to div if not specified
         */
        constructor(type?: string, element?: HTMLElement) {
            if (!element) {
                this.element = document.createElement(type || "div");
            } else {
                this.element = element;
            }
        }

        /**
         * Adds a additional class to element
         * @param element element passed in which the new class is to be added to
         * @param className name of the css class that is being added
         */
        public addClass(className: string) {

            if (!this.element.classList.contains(className)) {
                this.element.classList.add(className);
            }
        }

        public removeClass(className: string) {
            if (this.element.classList.contains(className)) {
                this.element.classList.remove(className);
            }
        }

        public appendChild(element: BaseElement) {
            this.element.appendChild(element.element);
        }

        public removeChild(element: BaseElement) {
            this.element.removeChild(element.element);
        }

        public addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions) {
            this.element.addEventListener(type, listener, options);
        }

        public removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions) {
            this.element.removeEventListener(type, listener, options);
        }

    }
}
