namespace namespaceErrorPage {
    import BaseElement = namespaceCoreModule.BaseElement;
    import ImageElement = namespaceCoreModule.ImageElement;
    import TextLabel = namespaceCoreModule.TextLabel;
    import Page = namespaceCoreModule.Page;
    export class ErrorPage extends Page {

        protected getStyleList(): string[] {
            return [
            "ErrorPage/ErrorPage.css",
            ];
        }

        protected build() {

            const errorContent = new TextLabel();
            errorContent.addClass("ErrorPage_Content");
            errorContent.setText("Somehow you have managed to navigate to a page that does not exist");
            this.appendChild(errorContent);

            this.addClass("ErrorPage");
            const imageContainer = new BaseElement();
            imageContainer.addClass("ErrorPage_ImageContainer");
            const youAreLost = new ImageElement();
            youAreLost.addClass("ErrorPage_LostImage");
            youAreLost.source("img/lost.jpg");
            imageContainer.appendChild(youAreLost);
            this.appendChild(imageContainer);
        }
    }
}
