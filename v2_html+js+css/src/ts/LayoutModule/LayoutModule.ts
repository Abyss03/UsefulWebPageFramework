namespace Layout {
    import Loader = namespaceLoader.Loader;
    import BaseElement = namespaceCoreModule.BaseElement;
    import TopBarModule = namespaceTopBar.TopBarModule;
    import Page = namespaceCoreModule.Page;
    import KeyedCollection = namespaceCoreModule_Types.KeyedCollection;

    export class LayoutModule {

        private pageHash: string;
        private pageName: string;
        private currentPage: Page;
        private centerContent: BaseElement;
        private loader: Loader = new Loader();
        private loadedJavaScript: HTMLScriptElement[] = [];
        private pageLookup: KeyedCollection<string> = new KeyedCollection([
            ["home", "HomePage"],
        ]);
        private additionalJsFileCount = 0;
        private additionalJsFilesLoaded = 0;

        /**
         * Called upon original page load, creates the main layout framework
         * @param pageHash if the page has an existing hash then load that, otherwise
         */
        public instantiatePage(pageHash?: string): void {

            const body = new BaseElement(null, document.body);
            const siteContent = new BaseElement();
            siteContent.addClass("SiteContent");
            body.appendChild(siteContent);

            // create header
            const topBar = new TopBarModule();
            siteContent.appendChild(topBar);
            // create center content
            this.centerContent = new BaseElement();
            this.centerContent.addClass("CenterContent");
            siteContent.appendChild(this.centerContent);
            // navigate to the users current page
            this.navigate(pageHash || "home");
        }

        /**
         * changes the center content module to be the new page based on the new hash.
         * @param pageHash the page hash taken from the url
         * @param local if the link is local or not, if not then take the user off to that page.
         */
        public navigate(pageHash: string, local: boolean = true): void {

            if (local) {
                this.pageHash = pageHash.replace("#", "");
                this.pageHash = this.pageHash.replace(/\//g, "");
                this.pageName = this.getPageModuleName(this.pageHash);

                this.retrieveJavascriptAndAddToScreen();

            } else {
                window.location.href = pageHash;
            }
        }

        /**
         * When a new central content page is loaded, the new javascript files need to be loaded before
         * it can be appended to the screen. Otherwise we cannot load the new content.
         */
        private retrieveJavascriptAndAddToScreen(): void {

            const srcLocation = `out/${this.pageName}/${this.pageName}.js`;
            let isLoaded: boolean = false;
            for (const loadedJsFile of this.loadedJavaScript) {
                if (loadedJsFile.src.indexOf(srcLocation) > -1) {
                    isLoaded = true;
                    break;
                }
            }

            if (isLoaded) {
                this.getFilesCleanAndAddElements();
                return;
            }

            const jsLink = document.createElement("script");
            jsLink.type = "text/javascript";
            jsLink.src = srcLocation;
            jsLink.onload = () => {
                this.getFilesCleanAndAddElements();
            };
            document.head.appendChild(jsLink);
            this.loadedJavaScript.push(jsLink);
        }

        private getFilesCleanAndAddElements() {
            const pageType: typeof Page = this.loader.getPage(this.pageName);
            const additionalFiles = pageType.getJavascriptFiles();
            if (additionalFiles && additionalFiles.length > 0) {
                this.additionalJsFileCount = additionalFiles.length;
                for (const file of additionalFiles) {
                    const jsLink = document.createElement("script");
                    jsLink.type = "text/javascript";
                    jsLink.src = file;
                    jsLink.onload = () => {
                        this.checkBeforeCleanAndAdd(pageType);
                    };
                    document.head.appendChild(jsLink);
                    this.loadedJavaScript.push(jsLink);
                }
            } else {
                this.cleanAndAddElement(pageType);
            }
        }

        private checkBeforeCleanAndAdd(pageType: typeof Page) {
            this.additionalJsFilesLoaded += 1;
            if (this.additionalJsFilesLoaded === this.additionalJsFileCount) {
                this.additionalJsFilesLoaded = 0;
                this.cleanAndAddElement(pageType);
            } else {
                return;
            }
        }

        private cleanAndAddElement(pageType: typeof Page) {

            const newPage = new pageType();

            if (this.currentPage && (newPage.constructor as any).name === (this.currentPage.constructor as any).name) {
                return;
            }

            if (this.currentPage) {
                this.currentPage.cleanUp();
                this.centerContent.removeChild(this.currentPage);
            }

            // history.pushState(null, null, "#" + this.pageHash);
            this.currentPage = new pageType();
            this.centerContent.appendChild(this.currentPage);
        }

        private getPageModuleName(pageHash: string): string {

            const pageName = this.pageLookup.Item(pageHash);
            if (!pageName) {
                // tslint:disable-next-line: no-console
                console.error(`No Page Type was found for the following hash: ${pageHash}`);
                return "ErrorPage";
            }
            return pageName;
        }
    }
}
